from dataclasses import dataclass
from pygame.sprite import Sprite
from esper import Processor
from platformer.core.game import GameObject


@dataclass
class SpriteComponent:
    sprite: Sprite


class SpriteProcessor(Processor):
    def process(self, scene, delta):
        for _, (map_object, sprite_component) in self.world.get_components(
            GameObject, SpriteComponent
        ):
            sprite = sprite_component.sprite
            sprite.rect = map_object.rect
