import os

import pygame
from esper import World

from platformer.core.scenes import Scene
from platformer.core.map import MapLoader
from platformer.core.player import Player
from platformer.core.processor import ProcessorFactory


class LevelScene(Scene):
    def __init__(self):
        super(LevelScene, self).__init__("level")
        self.world = None
        self.map = None
        self.player = None
        self.processor_factors = None

    def setup(self):
        self.world = World()
        self.map = MapLoader(os.path.join("assets", "maps", "map.tmx"), self.world)
        self.player = Player(self)
        self.processor_factors = ProcessorFactory(self.world)

    def teardown(self):
        self.world.clear_database()

    def resume(self):
        pass

    def draw(self, surface):
        self.map.draw(surface)
        pygame.display.flip()

    def update(self, delta, events):
        self.handle_input(events)
        self.world.process(self, delta)

    def clear(self, surface):
        pass

    def handle_input(self, events):
        for event in events:
            if event.type in [pygame.KEYDOWN, pygame.KEYUP]:
                self.handle_key(event)
            elif event.type in [pygame.QUIT]:
                self.exit_game()

    def handle_key(self, event):
        if event.type == pygame.KEYDOWN:
            if event.key in [pygame.K_a, pygame.K_d]:
                self.player.start_walking(event.key)
        elif event.type == pygame.KEYUP:
            if event.key in [pygame.K_a, pygame.K_d]:
                self.player.stop_walking(event.key)
            elif event.key in [pygame.K_SPACE]:
                self.player.jump()
            elif event.key in [pygame.K_ESCAPE]:
                self.exit_game()

    def exit_game(self):
        self.game.pop_scene()
