import argparse
import logging

import pygame

from .core.scenes import Game
from .scenes import LevelScene


logger = logging.getLogger(__name__)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-m", "--map", type=str, help="Which Map to load", default="map.tmx"
    )
    parser.add_argument(
        "-d", "--debug", help="Activate Debug Mode", default=False, action="store_true"
    )
    return parser.parse_args()


def bootstrap_game():
    logging.basicConfig(level=logging.ERROR)

    pygame.init()

    main_surface = pygame.display.set_mode((1280, 960))

    game = Game(60, main_surface)
    game.register_scene(LevelScene())

    game.push_scene("level")
    return game
