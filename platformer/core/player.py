from dataclasses import dataclass
from pygame.sprite import Sprite
from pygame.surface import Surface
from esper import Processor
from planar import Vec2

from platformer.core.game import GameObject
from platformer.gfx.sprite import SpriteComponent


PLAYER_SPEED = 240
JUMP_SPEED = PLAYER_SPEED * 4


@dataclass
class PlayerComponent:
    name: str = "Player"


@dataclass
class MovementMarker:
    """ Marker class to specify if Player is moving """


class PlayerMovementProcessor(Processor):
    def process(self, scene, delta):
        pass


class Player:
    def __init__(self, scene):
        self.world = scene.world
        player_object = next(iter(scene.map.objects["Player"]))
        sprite_group = scene.map.sprite_group

        object_component = self.world.component_for_entity(player_object, GameObject)
        sprite = self.create_player_sprite(object_component, sprite_group)
        self.component_id = self.world.create_entity(
            PlayerComponent(), SpriteComponent(sprite), object_component
        )

    @staticmethod
    def create_player_sprite(obj, sprite_group):
        sprite = Sprite()
        sprite_group.add(sprite, layer=10)
        sprite.rect = obj.rect
        surface = Surface(sprite.rect.size)
        sprite.image = surface.convert_alpha()
        sprite.image.fill((255, 0, 0))

        return sprite
