from dataclasses import dataclass
from planar import Point
from pygame import Rect


@dataclass
class GameObject:
    pos: Point
    size: tuple
    properties: dict = None

    @property
    def w(self):
        return self.size[0]

    @property
    def h(self):
        return self.size[1]

    @property
    def rect(self):
        return Rect(*self.pos, *self.size)

    @rect.setter
    def rect(self):
        raise NotImplementedError("Set position not rect directly!")
