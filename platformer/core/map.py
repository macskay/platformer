import pytmx
import pyscroll
from planar import Point

from platformer.core.game import GameObject


class MapLoader:
    def __init__(self, path, world):
        self.world = world
        self.tiled_data = pytmx.load_pygame(path)
        self.data = pyscroll.TiledMapData(self.tiled_data)
        self.layer = pyscroll.BufferedRenderer(self.data, (1280, 960), alpha=True)
        self.objects = self.load_objects()
        self.sprite_group = pyscroll.PyscrollGroup(map_layer=self.layer)

    def draw(self, surface):
        self.sprite_group.draw(surface)

    def load_objects(self):
        objects = {}
        for layer in self.tiled_data.objectgroups:
            objects[layer.name] = []
            for obj in layer:
                obj.name = layer.name
                objects[layer.name].append(self.create_object_entity(obj))
        return objects

    def create_object_entity(self, obj):
        pos = Point(obj.x, obj.y)
        size = (obj.width, obj.height)
        properties = getattr(obj, "properties", None)
        component = GameObject(pos, size, properties=properties)
        return self.world.create_entity(component)
