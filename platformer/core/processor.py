from platformer.gfx.sprite import SpriteProcessor
from platformer.physics.gravity import GravityProcessor
from platformer.physics.collision import CollisionProcessor


class ProcessorFactory:
    def __init__(self, world):
        self.world = world
        self.setup()

    def setup(self):
        self.world.add_processor(SpriteProcessor())
        self.world.add_processor(GravityProcessor())
        self.world.add_processor(CollisionProcessor())
