from esper import Processor
from planar import Vec2

from platformer.core.player import PlayerComponent
from platformer.core.game import GameObject


class GravityProcessor(Processor):
    def process(self, scene, delta):
        player_id, player = next(iter(self.world.get_component(PlayerComponent)))
        map_object = self.world.component_for_entity(player_id, GameObject)
        map_object.pos -= Vec2(0, -1) * 9.81
