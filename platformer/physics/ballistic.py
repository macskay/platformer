from dataclasses import dataclass
import math

from esper import Processor
from planar import Point, Vec2

GRAVITY_CONSTANT = 2000


@dataclass
class BallisticMotionComponent:
    start_pos: Point
    start_velocity: float
    elapsed_ms: float = 0.0

    @property
    def v0(self):
        return self.start_velocity

    @property
    def t(self):
        return self.elapsed_ms / 1000.0

    @property
    def beta(self):
        return math.radians(self.angle)

    @property
    def angle(self):
        return Vec2(0, 0).angle_to(self.start_velocity)

    @property
    def peak(self):
        return Point(
            (
                math.sin(self.beta)
                * math.cos(self.beta)
                * ((self.v0.x * self.v0.x) / GRAVITY_CONSTANT)
            ),
            ((self.v0.y * self.v0.y) * (math.sin(self.beta) ** 2))
            / (2 * GRAVITY_CONSTANT),
        )

    @property
    def reach(self):
        return Point(
            ((self.v0.x * self.v0.x) * math.sin(2 * self.beta)) / GRAVITY_CONSTANT, 0.0
        )

    @property
    def _rt(self):
        rt = Vec2(
            self.v0.x * self.t * math.cos(self.beta),
            (self.v0.y * self.t * math.sin(self.beta))
            - (0.5 * GRAVITY_CONSTANT * self.t ** 2),
        )
        return rt

    @property
    def current_pos(self):
        return Point(self.start_pos.x + self._rt.x, self.start_pos.y - self._rt.y)

    @property
    def duration(self):
        return 2.0 * (self.v0.y * (math.sin(self.beta) / GRAVITY_CONSTANT)) * 1000.0

    @property
    def vt(self):
        return Vec2(
            self.v0.x * math.cos(self.beta),
            self.v0.y * math.sin(self.beta) - GRAVITY_CONSTANT * self.t,
        )


class BallisticMotionProcessor(Processor):
    def process(self, scene, delta):
        ballistic_components = self.world.get_component(BallisticMotionComponent)
        for _, component in ballistic_components:
            component.elapsed_ms += delta
