from esper import Processor
from planar import Point

from platformer.core.player import PlayerComponent
from platformer.core.game import GameObject


class CollisionProcessor(Processor):
    def process(self, scene, delta):
        _, (player_component, player_object) = next(
            iter(self.world.get_components(PlayerComponent, GameObject))
        )
        for collision_id in scene.map.objects["Collision"]:
            collision_object = self.world.component_for_entity(collision_id, GameObject)
            if player_object.rect.colliderect(collision_object.rect):
                if player_object.rect.centery <= collision_object.rect.centery:
                    player_object.pos = Point(
                        player_object.pos.x,
                        collision_object.rect.top - player_object.rect.height,
                    )
