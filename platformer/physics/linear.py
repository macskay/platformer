from dataclasses import dataclass

from esper import Processor
from planar import Point

GRAVITY_CONSTANT = 9.81


@dataclass
class LinearMotionComponent:
    start_pos: Point
    start_velocity: float
    elapsed_ms: float = 0.0

    @property
    def v0(self):
        return self.start_velocity

    @property
    def t(self):
        return self.elapsed_ms / 1000.0

    @property
    def _rt(self):
        return self.v0 * self.t

    @property
    def current_pos(self):
        return Point(self.start_pos.x + self._rt.x, self.start_pos.y + self._rt.y)


class LinearMotionProcessor(Processor):
    def process(self, scene, delta):
        linear_components = self.world.get_component(LinearMotionComponent)
        for _, component in linear_components:
            component.elapsed_ms += delta
