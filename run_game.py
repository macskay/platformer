import pygame  # wtf does this do?
import pygame.mixer

from platformer.bootstrap import bootstrap_game


if __name__ == "__main__":
    try:
        game = bootstrap_game()
        game.loop()

        pygame.mixer.music.stop()
    except Exception:
        pygame.quit()
        raise
